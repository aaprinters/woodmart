<?php
/**
 * Enqueue script and styles for child theme
 */

function woodmart_child_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'woodmart-style' ),time() );
    wp_enqueue_style( 'fa-style', get_stylesheet_directory_uri() . '/font-awesome-new.css','', array());
	wp_enqueue_script( 'penci', get_template_directory_uri() . '-child/js/owl.carousel1.js', array( 'jquery' ), time(), true );
	wp_enqueue_script( 'sticky-sidebar', get_template_directory_uri() . '-child/js/sticky-sidebar.js', array( 'jquery' ), time(), true );
	$localize_script = array(
		'ajaxUrl'         => admin_url( 'admin-ajax.php' ),
		'nonce'           => wp_create_nonce( 'ajax-nonce' ),
		'errorMsg'        => esc_html__( 'Something wrong happened. Please try again.', 'pennews' ),
		'login'           => 'penci_plogin_email_place',
		'password'        => 'penci_plogin_pass_place',
		'errorPass'       => '<p class="message message-error">' . 'penci_plogin_mess_error_email_pass' . '</p>',
		'prevNumber'      =>  'penci_autoload_prev_number',
		'minlengthSearch' => 'penci_ajaxsearch_minlength',
		'linkTitle'       => 'http://dev2.myreplicachoice.com' ,
		'linkTextAll'     => 'fasfhasdf s adhjks',
		'linkText'        => 'asdfasdf',
	);
	wp_localize_script( 'penci', 'PENCILOCALIZE', $localize_script );
	wp_enqueue_script( 'woodmart-theme', get_template_directory_uri() . '-child/js/functions.min.js', array( 'js-cookie' ), time(), true );

}
add_action( 'wp_enqueue_scripts', 'woodmart_child_enqueue_styles', 1000 );


function penci_get_post_author( $show = true, $has_icon = true ) {
	$byline = '<span class="entry-meta-item penci-byline">by <span class="author vcard"><a class="url fn n penci_pmeta-link" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span></span>';

	$output = '<span class="entry-meta-item penci-byline">';
	$output .= $byline;
	$output .= '</span>';

	if( ! $show ) {
		return $output;
	}

	echo ( $output );
}

function penci_get_post_date( $show = true ) {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$show_updated = penci_get_theme_mod( 'penci_show_date_updated' );

	$time_string = sprintf( $time_string,
		! $show_updated ? esc_attr( get_the_date( 'c' ) ) : esc_attr( get_the_modified_date( 'c' ) ),
		! $show_updated ? esc_html( get_the_date() ) : esc_html( get_the_modified_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'pennews' ),
		'<a class="penci_pmeta-link"  href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$output = '<span class="entry-meta-item penci-posted-on"><i class="fa fa-clock-o"></i>' . $time_string . '</span>'; // WPCS: XSS OK.

	if( ! $show ) {
		return $output;
	}

	echo ( $output );
}
function penci_get_comment_count( $show = true ) {

	$output ='<span class="entry-meta-item penci-comment-count">';
	$output .='<a class="penci_pmeta-link" href="'. esc_url( get_comments_link() ) . '"><i class="la la-comments"></i>';
	$output .= get_comments_number_text( esc_html__( '0', 'pennews' ), esc_html__( '1', 'pennews' ), '% ' . esc_html__( '', 'pennews' ) );
	$output .='</a></span>';

	if( ! $show ) {
		return $output;
	}

	echo ( $output );

}
if( !function_exists( 'penci_get_post_countview' ) ) {
	function penci_get_post_countview( $post_id, $show = false ) {

		$count = (int) get_post_meta( $post_id, '_count-views_all', true );

		$output = '<span class="entry-meta-item penci-post-countview penci_post-meta_item">';
		$output .= '<i class="fa fa-eye"></i><span>' . $count . '</span>';
		$output .= '</span>';

		if ( $show ) {
			echo $output;
		}

		return $output;
	}
}
/**
 * Social buttons class.
 */

function penci_get_social_share( $show =  true, $popup = false ) {
	$target = '_blank';
	$page_link = get_the_permalink();
	if ( woodmart_woocommerce_installed() && is_shop() ) $page_link = get_permalink( get_option( 'woocommerce_shop_page_id' ) );
	if ( woodmart_woocommerce_installed() && ( is_product_category() || is_category() ) ) $page_link = get_category_link( get_queried_object()->term_id );
	if ( is_home() && ! is_front_page() ) $page_link = get_permalink( get_option( 'page_for_posts' ) );
	$output = '';
	//if( $markup_social_share ) {
		$output .=  '<span class="social-buttons">';
		$output .=  '<span class="social-buttons__content">
		<a class="penci-social-item facebook" target="_blank" rel="noopener" title="" href="https://www.facebook.com/sharer/sharer.php?u='. $page_link.'"><i class="fa fa-facebook"></i></a>
		<a class="penci-social-item twitter" rel="nofollow" href="https://twitter.com/share?url=' . $page_link.'" target="'.$target.'">
		<i class="fa fa-twitter"></i>
		</a>
		<a class="penci-social-item pinterest" target="'.$target.'" rel="noopener" title="" href="http://pinterest.com/pin/create/button?url='. $page_link .'"><i class="fa fa-pinterest"></i></a>
		<a class="penci-social-item email" target="'.$target.'" rel="noopener" href="mailto:?subject=Check%20this%20'. $page_link.'"><i class="fa fa-envelope"></i></a>
		</span>';

		if( $popup ) {
			$output .= '<a class="social-buttons__toggle" href="#"><i class="fa fa-share"></i></a>';
		}

		$output .= '</span>';
	//}

	if( ! $show ) {
		return $output;
	}

	echo ( $output );

}

/* if ( ! function_exists( 'penci_get_setting' ) ):
	function penci_get_setting( $name , $type = '' ) {

		if( 'color' == $type ){
			$value = penci_get_theme_mod( $name, penci_colors_default_setting( $name ) );
		}else{
			$value = penci_get_theme_mod( $name, penci_default_setting( $name ) );
		}
		return do_shortcode( $value );

	}
endif; */


if ( ! function_exists( 'penci_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 */
	function penci_setup() {

		$dis_thumb_480_645   = get_theme_mod( 'penci_dis_thumb_480_645' );
		$dis_thumb_480_480   = get_theme_mod( 'penci_dis_thumb_480_480' );
		$dis_thumb_480_320   = get_theme_mod( 'penci_dis_thumb_480_320' );
		$dis_thumb_280_376   = get_theme_mod( 'penci_dis_thumb_280_376' );
		$dis_thumb_280_186   = get_theme_mod( 'penci_dis_thumb_280_186' );
		$dis_thumb_280_280   = get_theme_mod( 'penci_dis_thumb_280_280' );
		$dis_thumb_760_570   = get_theme_mod( 'penci_dis_thumb_760_570' );
		$dis_thumb_1920_auto = get_theme_mod( 'penci_dis_thumb_1920_auto' );
		$dis_thumb_960_auto  = get_theme_mod( 'penci_dis_thumb_960_auto' );
		$dis_thumb_auto_400  = get_theme_mod( 'penci_dis_thumb_auto_400' );
		$dis_thumb_585_auto  = get_theme_mod( 'penci_dis_thumb_585_auto' );
		$hide_sidebar_editor  = get_theme_mod( 'penci_hide_sidebar_editor' );


		add_image_size( 'penci-thumb-480-645', 480, 645, true );
		add_image_size( 'penci-thumb-480-480', 480, 480, true );
		add_image_size( 'penci-thumb-760-570', 760, 570, true );
		add_image_size( 'penci-thumb-480-320', 480, 320, true );
		add_image_size( 'penci-thumb-1920-auto', 1920, 999999, false );
		add_image_size( 'penci-thumb-960-auto', 960, 999999, false );
		add_image_size( 'penci-thumb-auto-400', 999999, 400, false );
		add_image_size( 'penci-masonry-thumb', 585, 99999, false );
/* 		if ( ! $dis_thumb_280_376 ): add_image_size( 'penci-thumb-280-376', 280, 376, true ); endif;
		if ( ! $dis_thumb_280_186 ): add_image_size( 'penci-thumb-280-186', 280, 186, true ); endif;
		if ( ! $dis_thumb_280_280 ): add_image_size( 'penci-thumb-280-280', 280, 280, true ); endif; */
	}
endif;
add_action( 'after_setup_theme', 'penci_setup' );

/**
 * Register widget area.
 *
 */
function penci_widgets_init() {
	$sidebars = array(
		'sidebar-1' => esc_html__( 'Sidebar Right', 'pennews' ),
		'sidebar-2' => esc_html__( 'Sidebar Left', 'pennews' ),
	);

	$sidebars_footer = array(
		'footer-1' => esc_html__( 'Footer Column #1', 'pennews' ),
		'footer-2' => esc_html__( 'Footer Column #2', 'pennews' ),
		'footer-3' => esc_html__( 'Footer Column #3', 'pennews' ),
		'footer-4' => esc_html__( 'Footer Column #4', 'pennews' ),
	);

	$description = array(
		'sidebar-1' => esc_html__( 'Add widgets here to display them on blog and single', 'pennews' ),
		'sidebar-2' => esc_html__( 'Add widgets here to display them on page', 'pennews' ),
		'footer-1'  => esc_html__( 'Add widgets here to display them in the first column of the footer', 'pennews' ),
		'footer-2'  => esc_html__( 'Add widgets here to display them in the second column of the footer', 'pennews' ),
		'footer-3'  => esc_html__( 'Add widgets here to display them in the third column of the footer', 'pennews' ),
		'footer-4'  => esc_html__( 'Add widgets here to display them in the fourth column of the footer', 'pennews' ),
	);

	$class_before_widget = ' penci-block-vc penci-widget-sidebar';
	//$class_before_widget .= ' ' . penci_get_setting( 'penci_style_block_title' );
	//$class_before_widget .= ' ' . penci_get_setting( 'penci_align_blocktitle' );

	$before_widget = '<div id="%1$s" class="widget ' . $class_before_widget . ' %2$s">';
	$before_title  = '<div class="penci-block-heading"><h4 class="widget-title penci-block__title"><span>';
	$after_title   = '</span></h4></div>';


	foreach ( $sidebars as $sidebar => $name ) {
		register_sidebar( array(
			'name'          => $name,
			'id'            => $sidebar,
			'description'   => $description[ $sidebar ],
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title
		) );
	}

	register_sidebar( array(
		'name'          => esc_html__( 'Header Signup Form', 'pennews' ),
		'id'            => 'header-signup-form',
		'before_widget' => '<div id="%1$s" class="penci-block-vc penci-mailchimp mailchimp_style-3 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="header-signup-form">',
		'after_title'   => '</h4>',
		'description'   => esc_html__( 'Only use for MailChimp Sign-Up Form widget. Display your Sign-Up Form widget below the header. Please use markup we provide here: http://soledad.pencidesign.com/soledad-document/#widgets to display exact', 'pennews' ),
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Signup Form', 'pennews' ),
		'id'            => 'footer-signup-form',
		'before_widget' => '<div id="%1$s" class="footer-subscribe penci-mailchimp mailchimp_style-2 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="footer-subscribe-title">',
		'after_title'   => '</h4>',
		'description'   => esc_html__( 'Only use for MailChimp Sign-Up Form widget. Display your Sign-Up Form widget below on the footer. Please use markup we provide here: http://soledad.pencidesign.com/soledad-document/#widgets to display exact', 'pennews' ),
	) );


	$footer_col  = 4;
	//$footer_class_before = penci_get_setting( 'penci_fwidget_block_title_style' );
	//$footer_class_before .= ' ' . penci_get_setting( 'penci_fwidget_align_blocktitle' );

	$i = 1;
	foreach ( $sidebars_footer as $sidebar => $name ) {


	register_sidebar( array(
		'name'          => $footer_col > 1 ? $name : esc_html__( 'Footer Sidebar', 'pennews' ),
		'id'            => $sidebar,
		'description'   => $description[ $sidebar ],
		'before_widget' => '<div id="%1$s" class="widget footer-widget penci-block-vc penci-fwidget-sidebar ' . $footer_class_before . ' %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => $before_title,
		'after_title'   => $after_title
	) );

	$i ++;
	}

	if ( class_exists( 'WooCommerce' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar For Shop', 'pennews' ),
			'id'            => 'penci-shop-sidebar',
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title,
			'description'   => esc_html__( 'This sidebar for Shop Page & Shop Archive, if this sidebar is empty, will display Main Sidebar', 'pennews' ),
		) );
	}

	if ( class_exists( 'Penci_Portfolio' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Portfolio Sidebar Left', 'pennews' ),
			'id'            => 'penci-portfolio-sidebar-left',
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title,
			'description'   => esc_html__( 'This sidebar for Portfolio Detail, if this sidebar is empty, will display Main Sidebar', 'pennews' ),
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Portfolio Sidebar Right', 'pennews' ),
			'id'            => 'penci-portfolio-sidebar-right',
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title,
			'description'   => esc_html__( 'This sidebar for Portfolio Detail, if this sidebar is empty, will display Main Sidebar', 'pennews' ),
		) );
	}

	if ( class_exists( 'bbPress' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar bbPress', 'pennews' ),
			'id'            => 'penci-bbpress',
			'description'   => esc_html__( 'Add widgets here.', 'pennews' ),
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title
		) );
	}

	if ( class_exists( 'BuddyPress' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar BuddyPress', 'pennews' ),
			'id'            => 'penci-buddypress',
			'description'   => esc_html__( 'Add widgets here.', 'pennews' ),
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title
		) );
	}

	if ( class_exists( 'Tribe__Events__Main' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar Events', 'pennews' ),
			'id'            => 'penci-event',
			'description'   => esc_html__( 'Add widgets here.', 'pennews' ),
			'before_widget' => $before_widget,
			'after_widget'  => '</div>',
			'before_title'  => $before_title,
			'after_title'   => $after_title
		) );
	}

	if ( class_exists( 'Penci_Instagram' ) ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Footer Instagram', 'pennews' ),
			'id'            => 'footer-instagram',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="footer-instagram-title"><span><span class="title">',
			'after_title'   => '</span></span></h4>',
			'description'   => esc_html__( 'Only use for Instagram Slider widget. Display instagram images on your website footer', 'pennews' ),
		) );
	}

	register_sidebar( array(
		'name'          => esc_html__( 'Hamburger Widget Above', 'pennews' ),
		'id'            => 'menu_hamburger_1',
		'before_widget' => '<div id="%1$s" class="penci-menu-hbg-widgets widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="menu-hbg-title"><span>',
		'after_title'   => '</span></h4>',
		'description'   => esc_html__( 'Display widgets on above menu on menu hamburger', 'pennews' ),
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Hamburger Widget Below', 'pennews' ),
		'id'            => 'menu_hamburger_2',
		'before_widget' => '<div id="%1$s" class="penci-menu-hbg-widgets widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="menu-hbg-title"><span>',
		'after_title'   => '</span></h4>',
		'description'   =>esc_html__( 'Display widgets on below menu on menu hamburger', 'pennews' ),
	) );
}

add_action( 'widgets_init', 'penci_widgets_init',0 );


