<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 */

$sidebar_class = woodmart_get_sidebar_class();

$sidebar_name = woodmart_get_sidebar_name();

if ( strstr( $sidebar_class, 'col-lg-0' ) ) return;
?>
<script type="text/javascript">
	var isMobile = window.matchMedia("only screen and (max-width: 760px)");

    if (isMobile.matches) {
		console.log('mobile view');
        //Add your script that should run on mobile here
    }else{
		console.log('desktop view');
		if (jQuery("body").hasClass("admin-bar")) {
			var sidebar = new StickySidebar('#left_sidebar', {
				containerSelector: '.site-content',
				innerWrapperSelector: '.sidebar-inner',
				topSpacing:90,
				bottomSpacing:0
			});
		}else{
				var sidebar = new StickySidebar('#left_sidebar', {
				containerSelector: '.site-content',
				innerWrapperSelector: '.sidebar-inner',
				topSpacing:60,
				bottomSpacing:0
			});
		}
	}
</script>


<aside id="left_sidebar" class="sidebar-container col-lg-3 col-md-3 col-12 order-md-first sidebar-left area-<?php echo esc_attr( $sidebar_name ); ?>" role="complementary">
	<div class="widget-heading">
		<a href="#" class="close-side-widget"><?php esc_html_e( 'close', 'woodmart' ); ?></a>
	</div>
	<div class="sidebar-inner woodmart-sidebar-scroll">
		<div class="widget-area woodmart-sidebar-content">
			<?php do_action( 'woodmart_before_sidebar_area' ); ?>
			<?php dynamic_sidebar( $sidebar_name ); ?>
			<?php do_action( 'woodmart_after_sidebar_area' ); ?>
		</div><!-- .widget-area -->
	</div><!-- .sidebar-inner -->
</aside><!-- .sidebar-container -->

<?php
/* 
<aside class="widget-area widget-area-2 penci-sticky-sidebar penci-sidebar-widgets">
	<div class="theiaStickySidebar">
	<?php dynamic_sidebar($sidebar_name); ?>
	</div>
</aside><!-- #secondary --> */
?>